Changelog
========================================

### 1.1.0

**Additions**

- Changed colors for Typescript types so that one can differentiate what will exist and runtime and what will not.

**Chores**

- Updated dependencies.


### 1.0.0

**Additions**

- Added syntax colors for SonarLint plugin.

**Bug Fixes**

- Fixed problems view button color.
- Fixed VCS blame gutter colors.

**Chores**

- Updated dependencies.


### 1.0.0-beta.8

**Bug Fixes**

- Fixed tags color.


### 1.0.0-beta.7

**Bug Fixes**

- Fixed selection background inconsistencies and contrast issues.
- Fixed typo in UI theme that caused tree counter badge to appear incorrectly.


### 1.0.0-beta.6

- Updated dependencies.

**Additions**

- Updated to work with new UI.

**Bug Fixes**

- Fixed several color inconsistencies across different components.


### 1.0.0-beta.5

- Updated dependencies.

**Additions**

- set colors for memory indicator.

**Bug Fixes**

- fixed semantic highlighting issues.
- fixed scrollbar colors.


### 1.0.0-beta.4

- Fixed incorrect colors for scroll bars.


### 1.0.0-beta.3

- Updated scrollbar colors in color scheme to match theme.
- Updated matching braces styles.


### 1.0.0-beta.2

- Updated colors to work with latest updates to IDEs.


#### 1.0.0-beta.1

**Additions**

- minor tweaks to wrapping indicator color and modified icon color.
- added color chooser background color
- added ssh remote run successful connection label color
- implemented toggle button colors

**Bug Fixes**

- fixed checkbox colors - fixes #2
- fixed blue icon coloring - fixes #3
- fixed neon green title bar - fixes #4


#### 1.0.0-beta.0

- Initial beta release.
