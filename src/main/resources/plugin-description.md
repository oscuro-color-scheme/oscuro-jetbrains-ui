A dark UI theme & syntax color scheme with a focus on distinction between as many
elements as reasonably possible.

<span/>

If you find anything strange, please [log a bug][issues] with attached screenshots.


Contributing
-------------------------------------------------

Contributions are always welcome. :)



[issues]: https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/issues "Create a Ticket"
