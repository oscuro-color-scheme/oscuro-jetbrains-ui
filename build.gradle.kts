buildscript {
  configurations.all {
    resolutionStrategy.dependencySubstitution {
      substitute(module("com.overzealous:remark:1.1.0")).using(module("com.wavefront:remark:2023-07.07")).because("not available on maven central anymore")
    }
  }
}

plugins {
  id("org.jetbrains.intellij") version "1.17.4"
  id("org.kordamp.gradle.markdown") version "2.2.0"
}

group = "dev.bsara.oscuro"
version = "1.1.0"

repositories {
  mavenCentral()
}

intellij {
  version.set("2024.2.4")
  instrumentCode.set(false)
}

tasks {
  val copyPluginDescMarkdown by registering(Copy::class) {
    group = "documentation"

    from("$projectDir/src/main/resources")
    include("*.md")

    into("$buildDir/markdown")
  }

  markdownToHtml {
    dependsOn(copyPluginDescMarkdown)

    sourceDir = file("$buildDir/markdown")
    outputDir = file("$buildDir/html")
  }

  patchPluginXml {
    dependsOn(markdownToHtml)

    sinceBuild.set("191.0")
    untilBuild.set("")

    pluginDescription.set(provider {
      file("$projectDir/build/html/plugin-description.html").readText()
    })

    version.set(project.version.toString())
  }
}
