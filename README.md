Oscuro UI Theme (JetBrains IDE Theme & Color Scheme)
=========================================================================================

[![Plugin Link](https://img.shields.io/jetbrains/plugin/v/19759-oscuro-theme?style=flat&logo=jetbrains)](https://plugins.jetbrains.com/plugin/19759-oscuro-theme)
[![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat)][license]

> A dark UI theme & syntax color scheme with a focus on distinction between as many
> elements as reasonably possible for JetBrains IDEs.

[Changelog](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/blob/master/CHANGELOG.md)

_Icons used in screenshots: [Atom Material Icons](https://plugins.jetbrains.com/plugin/10044-atom-material-icons)_

![Screenshot with Java #1](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/ui-and-java-0.png "Screenshot with Java #1")

![Screenshot with Java #2](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/ui-and-java-1.png "Screenshot with Java #2")

![Screenshot with Markdown](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/ui-and-markdown.png "Screenshot with Markdown")

![Screenshot with JSX](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/ui-and-jsx.png "Screenshot with JSX")

![Screenshot of Settings Modal](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/ui-modal.png "Screenshot of Settings Modal")

![Screenshot of Diff Sample](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/diff.png "Screenshot of Diff Sample")

![Screenshot of VCS Annotations Sample](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/vcs.png "Screenshot of VCS Annotations Sample")


### Classic UI

![Screenshot with Java #1](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/classic-ui-and-java-0.png "Screenshot with Java #1")

![Screenshot with Java #2](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/classic-ui-and-java-1.png "Screenshot with Java #2")

![Screenshot with Markdown](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/classic-ui-and-markdown.png "Screenshot with Markdown")

![Screenshot with JSX](https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/-/raw/master/screenshots/classic-ui-and-jsx.png "Screenshot with JSX")



Contributing
-------------------------------------------------

Contributions are always welcome. :)


<br/>
<br/>


# License

ISC License (ISC)

Copyright (c) 2024, Brandon D. Sara (https://bsara.dev)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[bsara-home]: https://bsara.dev
[license]:    https://gitlab.com/oscuro-color-scheme/oscuro-jetbrains-ui/blob/master/LICENSE "License"
